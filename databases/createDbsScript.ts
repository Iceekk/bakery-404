import { SaleModel } from './../src/models/sales-model';
import nedb from 'nedb';
import { Product } from '../src/models/product';
import { Recipe } from '../src/models/recipe';
import { IProduct } from './../src/core/contracts/models/product';
import { Ingredient } from './../src/models/ingredient';

const inStockDb: nedb = new nedb({ filename: './inStock', autoload: true });
const salesDb: nedb = new nedb({ filename: './sales', autoload: true });
const ingredientsDb: nedb = new nedb({
  filename: './ingredients',
  autoload: true
});
const recipeDb: nedb = new nedb({ filename: './recipes', autoload: true });

// A function addProductToInStock(product: IProduct): void { inStockDb.insert(product); }

// A function removeProductFromInStock(product: IProduct): void { inStockDb.remove(product); }
function makeRecordInSalesDb(productsList: string[], billSum: number): void {
  // A const salesDb: nedb = remote.getGlobal('vars').sale;
  const saleModel: SaleModel = new SaleModel(productsList, billSum, new Date());
  salesDb.insert(saleModel);
}

function listProduct(): void {
  inStockDb.getAllData();
}

function addIngredientToIngredientsDb(ingredient: Ingredient): void {
  ingredientsDb.insert(ingredient);
}

function removeIngredientToIngredientsDb(ingredient: Ingredient): void {
  ingredientsDb.remove(ingredient);
}

function listIngredients(ingredient: Ingredient): void {
  ingredientsDb.getAllData();
}

function addRecipeToDb(recipe: Recipe): void {
  recipeDb.insert(recipe);
}

function removeRecipeToDb(recipe: Recipe): void {
  recipeDb.remove(recipe);
}

function listRecipes(): void {
  recipeDb.getAllData();
}

function changeQuantityOfIngredient(
  ingredientName: string,
  amount: number
): void {
  ingredientsDb.find(
    { _ingredientName: ingredientName },
    (err: string, docs: any) => {
      ingredientsDb.update(
        { _ingredientName: ingredientName },
        { $inc: { _ingredientQuantity: amount } },
        { upsert: true },
        () => {
          console.log('done');
        }
      );
    }
  );
}
// A changeQuantityOfIngredient('yoghurt', -10);
