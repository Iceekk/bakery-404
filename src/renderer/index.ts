// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
// tslint:disable-next-line:ordered-imports
import jquery from 'jquery';
// tslint:disable-next-line:no-import-side-effect
import 'popper.js';
import 'bootstrap';
import { container } from '../common/ioc.config';
import { TYPES } from '../common/types';
import { IEngine } from './../core/contracts/engine';

setTimeout(() => {
  jquery('[data-toggle="tooltip"]').tooltip({
    trigger: 'hover'
  });
}, 100);

const engine: IEngine = container.get<IEngine>(TYPES.IEngine);
engine.start();
