import { app, BrowserWindow } from 'electron';
import nedb from 'nedb';
import * as path from 'path';
import * as url from 'url';
// tslint:disable-next-line:import-name
import __basedir from '../basepath';

const ingredients: nedb = new nedb({
  filename: `${__basedir}/databases/ingredients`,
  autoload: true
});

const inStock: nedb = new nedb({
  filename: `${__basedir}/databases/inStock`,
  autoload: true
});
const recipes: nedb = new nedb({
  filename: `${__basedir}/databases/recipes`,
  autoload: true
});
const sales: nedb = new nedb({
  filename: `${__basedir}/databases/sales`,
  autoload: true
});
// tslint:disable-next-line:no-any
declare const global: any;

global.vars = {
  ingredients: ingredients,
  sale: sales,
  inStock: inStock,
  recipes: recipes
};

let mainWindow: Electron.BrowserWindow;
function createWindow(): void {
  // Create the browser window.
  mainWindow = new BrowserWindow({ fullscreen: true });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(
        __basedir,
        __dirname,
        '../../src/renderer/index.html'
      ),
      protocol: 'file:',
      slashes: true
    })
  );

  // Open the DevTools. mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // In an array if your app supports multi windows, this is the time
    // When you should delete the corresponding element.
    // tslint:disable-next-line:prefer-type-cast
    mainWindow = null as any;
  });
}

// This method will be called when Electron has finished
// Initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // To stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it"s common to re-create a window in the app when the
  // Dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app"s specific main process
// Code. You can also put them in separate files and require them here.
