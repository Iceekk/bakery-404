import { Container } from 'inversify';
import { IBaseReport } from '../core/contracts/core/engine/factories/base-report';
import { IWriter } from '../core/contracts/core/engine/providers/writer';
import { IEngine } from '../core/contracts/engine';
import { Engine } from '../core/engine/engine';
import { IngredientReport } from '../core/engine/factories/ingredient-report';
import { RecipeReport } from '../core/engine/factories/recipe-report';
import { SaleReport } from '../core/engine/factories/sale-report';
import { Controller } from './../core/engine/controlers/main-controller';
import { ProductReport } from './../core/engine/factories/product-report';
import { HTMLWriter } from './../core/engine/providers/html-writer';
import { MainService } from './../core/services/main-service';
import { TYPES } from './types';

const container: Container = new Container();

container.bind<IEngine>(TYPES.IEngine).to(Engine);
container.bind<Controller>(TYPES.Controller).to(Controller);
container.bind<IWriter>(TYPES.HTMLWriter).to(HTMLWriter);
container.bind<MainService>(TYPES.MainService).to(MainService);

container.bind<IBaseReport>(TYPES.SaleReport).to(SaleReport);
container.bind<IBaseReport>(TYPES.ProductReport).to(ProductReport);
container.bind<IBaseReport>(TYPES.IngredientReport).to(IngredientReport);
container.bind<IBaseReport>(TYPES.RecipeReport).to(RecipeReport);

export { container };
