import { ITypes } from './types-interface';

const TYPES: ITypes = {
  IEngine: Symbol.for('IEngine'),
  Controller: Symbol.for('Controller'),
  HTMLWriter: Symbol.for('HTMLWriter'),
  MainService: Symbol.for('MainService'),
  SaleReport: Symbol.for('SaleReport'),
  ProductReport: Symbol.for('ProductReport'),
  IngredientReport: Symbol.for('IngredientReport'),
  RecipeReport: Symbol.for('RecipeReport')
};

// tslint:disable-next-line:export-name
export { TYPES };
