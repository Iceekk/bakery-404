export interface ITypes {
  IEngine: symbol;
  Controller: symbol;
  HTMLWriter: symbol;
  MainService: symbol;
  SaleReport: symbol;
  ProductReport: symbol;
  IngredientReport: symbol;
  RecipeReport: symbol;
}
