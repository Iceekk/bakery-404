export enum ProductType {
  BREAD = 'BREAD',
  BANITSA = 'BANITSA',
  MUFFIN = 'MUFFIN',
  EASTER_CAKE = 'EASTER_CAKE'
}
