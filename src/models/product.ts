import { IProduct } from './../core/contracts/models/product';
import { ProductType } from './enums/product-type';
import { Recipe } from './recipe';

export class Product implements IProduct {
  private _productGroup: ProductType;
  private _productRecipe: Recipe;
  private _productPrice: number;
  private _productQuantity: number;

  constructor(productGroup: ProductType, productRecipe: Recipe, productPrice: number, productQuantity: number) {
    this._productGroup = productGroup;
    this._productRecipe = productRecipe;
    this.productPrice = productPrice;
    this.productQuantity = productQuantity;
  }

  public get productGroup(): ProductType {
    return this._productGroup;
  }

  public get productRecipe(): Recipe {
    return this._productRecipe;
  }

  public set productPrice(productPrice: number) {
    if (productPrice === null || productPrice < 0) {
      throw new Error('Product price is invalid!');
    }
    this._productPrice = productPrice;
  }

  public get productPrice(): number {
    return this._productPrice;
  }

  public get productQuantity(): number {
    return this._productQuantity;
  }

  public set productQuantity(productQuantity: number) {
    if (productQuantity === null || productQuantity < 0) {
      throw new Error('Product quantity is invalid!');
    }
    this._productQuantity = productQuantity;
  }

}
