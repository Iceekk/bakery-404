export class SaleModel {
  private _productsList: string[];
  private _totalSum: number;
  private _dateOfSale: Date;

  constructor(productsList: string[], totalSum: number, dateOfSale: Date) {
    this.productsList = productsList;
    this.totalSum = totalSum;
    this.dateOfSale = dateOfSale;
  }

  public get productsList(): string[] {
    return this._productsList;
  }

  public set productsList(productsList: string[]) {
    if (productsList.length === 0) {
      throw new Error('Products list is empty!');
    }
    this._productsList = productsList;
  }

  public get totalSum(): number {
    return this._totalSum;
  }

  public set totalSum(totalSum: number) {
    if (totalSum === null || totalSum < 0) {
      throw new Error('Invalid total sum!');
    }
    this._totalSum = totalSum;
  }

  public get dateOfSale(): Date {
    return this._dateOfSale;
  }

  public set dateOfSale(dateOfSale: Date) {
    this._dateOfSale = dateOfSale;
  }

}
