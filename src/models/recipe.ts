import { ProductType } from './enums/product-type';
import { RecipeType } from './enums/recipe-type';
import { Ingredient } from './ingredient';

export class Recipe {
  private readonly _typeRecipe: ProductType;
  private readonly _recipeName: RecipeType;
  private readonly _recipeIngrediants: Ingredient[];

  constructor(typeRecipe: ProductType, recipeName: RecipeType, recipeIngrediants: Ingredient[]) {
    this._typeRecipe = typeRecipe;
    this._recipeName = recipeName;
    this._recipeIngrediants = recipeIngrediants;
  }

  public get recipeName(): RecipeType {
    return this._recipeName;
  }

  public get recipeIngrediants(): Ingredient[] {
    return this._recipeIngrediants;
  }

  public get typeRecipe(): ProductType {
    return this._typeRecipe;
  }
}
