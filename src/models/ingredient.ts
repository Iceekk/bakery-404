export class Ingredient {

  private _ingredientName: string;
  private _ingredientQuantity: number;
  private _ingredientExpirationDate: Date;

  constructor(ingredientName: string, ingredientQuantity: number, ingredientExpirationDate: Date) {
    this.ingredientName = ingredientName;
    this.ingredientQuantity = ingredientQuantity;
    this.ingredientExpirationDate = ingredientExpirationDate;
  }

  public set ingredientName(ingredientName: string) {
    if (ingredientName === null || ingredientName.length < 3) {
      throw new Error('Ingredient name is invalid!');
    }
    this._ingredientName = ingredientName;
  }

  public get ingredientName(): string {
    return this._ingredientName;
  }

  public set ingredientQuantity(ingredientQuantity: number) {
    if (ingredientQuantity < 0) {
      throw new Error('Ingredient quantity is invalid!');
    }
    this._ingredientQuantity = ingredientQuantity;
  }

  public get ingredientQuantity(): number {
    return this._ingredientQuantity;
  }

  public set ingredientExpirationDate(ingredientExpirationDate: Date) {
    this._ingredientExpirationDate = ingredientExpirationDate;
  }

  public get ingredientExpirationDate(): Date {
    return this._ingredientExpirationDate;
  }

}
