import { remote } from 'electron';
import nedb from 'nedb';
import $ from 'jquery';
export function productionListFiller(option: string): void {
  const dB: nedb = remote.getGlobal('vars').recipes;

  $('#list-production').empty();
  let content: string = '<ul>';
  dB.find({ _recipeName: option }, (err: string, docs: any) => {
    if (err) {
      throw new Error(err);
    }
    docs[0]._recipeIngrediants.forEach((elem: any) => {
      content += `<li class="ingredient-li"><span class="ingredient-name">${
        elem._ingredientName
      }:</span>
            <span class="ingredient-quantity">${
              elem._ingredientQuantity
            }</span></li>`;
    });
    content += '</li>';
    $('#list-production').append(content);
  });
}
