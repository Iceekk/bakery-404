import jquery from 'jquery';
import { htmlHide, htmlShow } from './hideShowHtmlElements-service';

export function showReportsHtml(): void {
  htmlHide('.wrapper');
  htmlShow('#report-wrapper');
  jquery('#current_mode').text('Reports');
  htmlHide('.footer');
  htmlShow('#reports-footer');
}
