// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';

import { injectable } from 'inversify';
import { container } from '../../common/ioc.config';
import { TYPES } from '../../common/types';
import { IWriter } from '../contracts/core/engine/providers/writer';
import { IService } from '../contracts/main-service';
import { AddProductRecordToBill } from './add-product-to-bill-service';
import { createProduct } from './create-product-service';
import { htmlHide, htmlShow } from './hideShowHtmlElements-service';
import { makeRecordInSalesDb } from './make-record-to-sales-service';
import { productionIngredientMultiplier } from './production-Ingredient-Multiplier';
import { productionListFiller } from './production-List-Filler';
import { productionProductFiller } from './production-product-filler';
import { productionRecipeFiller } from './production-recipe-filler';
import { showPosHtml } from './show-pos-html';
import { showProductionHtml } from './show-production-html';
import { showReportsHtml } from './show-reports-html';
import { updateQuantityOfProduct } from './update-quantity-inStock-service';

@injectable()
export class MainService implements IService {
  private readonly writer: IWriter = container.get<IWriter>(TYPES.HTMLWriter);
  private billModifier: AddProductRecordToBill = new AddProductRecordToBill(
    this.writer
  );
  public addProductRecordToBill(event: string): void {
    this.billModifier.addRowToBill(event);
  }

  public htmlShow(htmlElementToShow: string): void {
    htmlShow(htmlElementToShow);
  }

  public htmlHide(htmlElementToHide: string): void {
    htmlHide(htmlElementToHide);
  }

  public showReportsHtml(): void {
    showReportsHtml();
  }

  public showProductionHtml(): void {
    showProductionHtml();
  }

  public showPosHtml(): void {
    showPosHtml();
  }

  public updateQuantityOfProduct(productName: string, quantity: number): void {
    updateQuantityOfProduct(productName, quantity);
  }

  public makeRecordInSalesDb(productsList: string[], billSum: number): void {
    makeRecordInSalesDb(productsList, billSum);
  }
  public productionProductFiller(): void {
    productionProductFiller();
  }

  public productionRecipeFiller(option: string): void {
    productionRecipeFiller(option);
  }

  public productionListFiller(option: string): void {
    productionListFiller(option);
  }

  public productionIngredientMultiplier(option: number): void {
    productionIngredientMultiplier(option);
  }
  public createProduct(): void {
    createProduct();
  }
}
