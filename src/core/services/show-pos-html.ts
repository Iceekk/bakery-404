import jquery from 'jquery';
import { htmlHide, htmlShow } from './hideShowHtmlElements-service';

export function showPosHtml(): void {
  htmlHide('.wrapper');
  htmlShow('#wrapper');
  jquery('#current_mode').text('Pos');
  htmlShow('#total-id');
  htmlHide('.footer');
  htmlShow('#pos-footer');
}
