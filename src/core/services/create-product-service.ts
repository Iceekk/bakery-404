// tslint:disable-next-line:import-name
import $ from 'jquery';
import { RecipeType } from '../../models/enums/recipe-type';
import { updateIngredientsQuantity } from './update-quantity-ingredients-service';
import { updateQuantityOfProduct } from './update-quantity-inStock-service';

export function createProduct(): void {
  const selectedProduct: string = $('#recipe-drpdwn option:selected').text();
  const requiredQuantity: number = +$('#production-quantity')
    .val()
    .toString();

  if (!(selectedProduct in RecipeType)) {
    alert('Y U NO SELECT RECIPE ლ(ಠ益ಠლ).');
    throw new Error('recipe is not valid');
  }
  if (requiredQuantity <= 0) {
    console.log(requiredQuantity);

    alert('please input valid quantity!');
    throw new Error('quantity is not valid');
  }
  $('.ingredient-li').each(
    (_: never, el: HTMLElement): void => {
      const ingredientToUpdate: string = el.firstChild.textContent.slice(0, -1);
      const ingredientQuantityToUpdate: number =
        +el.lastChild.textContent * requiredQuantity;
      updateIngredientsQuantity(
        ingredientToUpdate,
        0 - ingredientQuantityToUpdate
      );
    }
  );
  updateQuantityOfProduct(selectedProduct, +requiredQuantity);
}
