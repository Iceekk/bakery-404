import { remote } from 'electron';
import nedb from 'nedb';
import { SaleModel } from '../../models/sales-model';

export function makeRecordInSalesDb(
  productsList: string[],
  billSum: number
): void {
  const salesDb: nedb = remote.getGlobal('vars').sale;
  const saleModel: SaleModel = new SaleModel(productsList, billSum, new Date());
  salesDb.insert(saleModel);
}
