import jquery from 'jquery';
import { htmlHide, htmlShow } from './hideShowHtmlElements-service';

export function showProductionHtml(): void {
  htmlHide('.wrapper');
  htmlShow('#production-wrapper');
  jquery('#current_mode').text('Production');
  htmlHide('#total-id');
  htmlHide('.footer');
  htmlShow('#production-footer');
}
