import { remote } from 'electron';
import $ from 'jquery';
import nedb from 'nedb';
export function productionRecipeFiller(option: string): void {
  const dB: nedb = remote.getGlobal('vars').recipes;

  $('#recipe-drpdwn').empty();
  $('#recipe-drpdwn').append(`<option class="recipe-option">Recipe</option>`);
  dB.find({ _typeRecipe: option }, (err: string, docs: any) => {
    if (err) {
      throw new Error(err);
    }
    docs.forEach((elem: any) => {
      $('#recipe-drpdwn').append(
        `<option class="recipe-option">${elem._recipeName}</option>`
      );
    });
  });
}
