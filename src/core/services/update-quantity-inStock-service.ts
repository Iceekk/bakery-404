import { remote } from 'electron';
import nedb from 'nedb';

export function updateQuantityOfProduct(
  productName: string,
  quantity: number
): void {
  const inStockDb: nedb = remote.getGlobal('vars').inStock;

  inStockDb.find(
    { _productRecipe: productName },
    (err: string, docs: object[]) => {
      if (err) {
        console.log(docs);
        throw new Error(err);
      }

      inStockDb.update(
        { _recipeName: productName },
        { $inc: { _productQuantity: quantity } },
        { upsert: false }
      );
    }
  );
}
