import jquery from 'jquery';
const $: JQueryStatic = jquery;

function htmlShow(htmlElementToShow: string): void {
  $(htmlElementToShow).show();
}
function htmlHide(htmlElementToHide: string): void {
  $(htmlElementToHide).hide();
}

export { htmlShow, htmlHide };
