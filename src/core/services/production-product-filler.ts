import $ from 'jquery';
import { ProductType } from '../../models/enums/product-type';

export function productionProductFiller(): void {
  $('#product-drpdwn').html('');
  $('#product-drpdwn').append(
    `<option class="product-option">Product</option>`
  );
  Object.keys(ProductType).forEach((product: string) => {
    $('#product-drpdwn').append(
      `<option class="product-option">${product}</option>`
    );
  });
}
