import $ from 'jquery';
export function productionIngredientMultiplier(option: number): void {
  $('#multiplied-ingredients').empty();
  $('.ingredient-li').each(
    (_: never, element: HTMLElement): void => {
      const result: number =
        +$(element).children('.ingredient-quantity')[0].textContent * option;
      $('#multiplied-ingredients').append(
        `<li class="ingrMultiplier"> X ${option} = ${result}</li>`
      );
    }
  );
}
