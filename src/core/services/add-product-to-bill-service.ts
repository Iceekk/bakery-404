// tslint:disable-next-line:no-import-side-effect
import jquery from 'jquery';
// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { IWriter } from '../contracts/core/engine/providers/writer';
import { billTemplate } from '../engine/providers/template';

export class AddProductRecordToBill {
  private readonly writer: IWriter;

  constructor(writer: IWriter) {
    this.writer = writer;
  }
  public addRowToBill(event: string): void {
    const [productNameInBill, productPriceInBill] = event.split('<br>');
    this.checkSameRecordInBill(
      productNameInBill,
      productPriceInBill,
      this.writer
    );
    this.resetHtmlInputValue('multiplier');
  }
  public getValueFromHtmlInputField(inputId: string): number {
    const inputElement: JQuery<HTMLElement> = jquery(`#${inputId}`);

    return inputElement.val() >= 1 ? +inputElement.val() : 1;
  }
  public updateExistingRow(productName: string, quantity: number): void {
    const currentQuantity: number = +jquery(`#${productName}`)
      .children('.product_quantity_in_bill')
      .text();
    jquery(`#${productName}`)
      .children('.product_quantity_in_bill')
      .text(currentQuantity + quantity);
  }
  public resetHtmlInputValue(elementId: string): void {
    jquery(`#${elementId}`).val(0);
  }

  public checkSameRecordInBill(
    productName: string,
    productPrice: string,
    writer: IWriter
  ): void {
    const quantity: number = this.getValueFromHtmlInputField('multiplier');
    if (jquery('#bill').children(`#${productName}`).length > 0) {
      this.updateExistingRow(productName, quantity);
    } else {
      this.insertNewRecord(productName, productPrice, quantity, writer);
      this.resetHtmlInputValue('multiplier');
    }
  }
  public insertNewRecord(
    productName: string,
    productPrice: string,
    productQuantity: number,
    writer: IWriter
  ): void {
    const content: string = billTemplate(
      productName,
      +productPrice.slice(0, -2),
      productQuantity
    );
    writer.write('bill', content);
  }
}
