import { remote } from 'electron';
import nedb from 'nedb';

export function updateIngredientsQuantity(
  productName: string,
  quantity: number
): void {
  const inStockDb: nedb = remote.getGlobal('vars').ingredients;

  inStockDb.find(
    { _productRecipe: productName },
    (err: string, docs: object[]) => {
      if (err) {
        console.log(docs);
        throw new Error(err);
      }

      inStockDb.update(
        { _ingredientName: productName },
        { $inc: { _ingredientQuantity: quantity } },
        { upsert: false }
      );
    }
  );
}
