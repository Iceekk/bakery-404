import { remote } from 'electron';
import { injectable } from 'inversify';
import nedb from 'nedb';
import { productsTemplate } from '../providers/template';
import { BaseReport } from './abstract/base-report';

@injectable()
export class ProductReport extends BaseReport {

    public getReport(): void {

        const inStockDb: nedb = remote.getGlobal('vars').inStock;
        // tslint:disable-next-line:no-any
        inStockDb.find({}, (err: string, docs: any) => {

            if (err) {
                throw new Error(err);
            }

            let content: string = '';
            // tslint:disable-next-line:no-any
            docs.forEach((elem: any) => {
                content += productsTemplate(elem._recipeName, elem._productQuantity);
            });
            this.writer.write('report_container_id', content);

        });
    }

}
