import { inject, injectable } from 'inversify';
import { TYPES } from '../../../../common/types';
import { IWriter } from '../../../contracts/core/engine/providers/writer';
import { IBaseReport } from './../../../contracts/core/engine/factories/base-report';

@injectable()
export abstract class BaseReport implements IBaseReport {

    private readonly _writer: IWriter;

    constructor(@inject(TYPES.HTMLWriter) writer: IWriter) {
        this._writer = writer;
    }

    public get writer(): IWriter {
        return this._writer;
    }

    public abstract getReport(): void;

}
