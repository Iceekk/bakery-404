import { remote } from 'electron';
import { injectable } from 'inversify';
import nedb from 'nedb';
import { recipeTemplate } from '../providers/template';
import { BaseReport } from './abstract/base-report';

@injectable()
export class RecipeReport extends BaseReport {

    public getReport(): void {

        const recipesDb: nedb = remote.getGlobal('vars').recipes;
        // tslint:disable-next-line:no-any
        recipesDb.find({}, (err: string, docs: any) => {

            if (err) {
                throw new Error(err);
            }

            let content: string = '';
            // tslint:disable-next-line:no-any
            docs.forEach((elem: any) => {

                const ingredients: string[] = elem._recipeIngrediants
                    // tslint:disable-next-line:no-any
                    .map((element: any) => `${element._ingredientName} ${element._ingredientQuantity}`);

                content += recipeTemplate(elem._recipeName, ingredients);

            });

            this.writer.write('report_container_id', content);

        });
    }

}
