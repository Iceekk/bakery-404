import { remote } from 'electron';
import { injectable } from 'inversify';
import nedb from 'nedb';
import { ingredientTemplate } from '../providers/template';
import { BaseReport } from './abstract/base-report';

@injectable()
export class IngredientReport extends BaseReport {

    public getReport(): void {

        const ingredientsDb: nedb = remote.getGlobal('vars').ingredients;
        // tslint:disable-next-line:no-any
        ingredientsDb.find({}, (err: string, docs: any) => {

            if (err) {
                throw new Error(err);
            }

            let content: string = '';
            // tslint:disable-next-line:no-any
            docs.forEach((elem: any) => {
                content += ingredientTemplate(elem._ingredientName, elem._ingredientQuantity);
            });
            this.writer.write('report_container_id', content);

        });
    }

}
