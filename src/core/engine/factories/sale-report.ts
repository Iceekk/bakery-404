import { remote } from 'electron';
import { injectable } from 'inversify';
import nedb from 'nedb';
import { salesTemplate } from '../providers/template';
import { BaseReport } from './abstract/base-report';

@injectable()
export class SaleReport extends BaseReport {

    public getReport(): void {

        const salesDb: nedb = remote.getGlobal('vars').sale;
        // tslint:disable-next-line:no-any
        salesDb.find({}, (err: string, docs: any) => {

            if (err) {
                throw new Error(err);
            }

            let content: string = '';
            // tslint:disable-next-line:no-any
            docs.forEach((elem: any) => {
                content += salesTemplate(elem._productsList, elem._totalSum, elem._dateOfSale);
            });
            this.writer.write('report_container_id', content);

        });
    }

}
