function buttonTemplate(name: string, price: number, quantity: number): string {
  return `<button class="product-btn btn btn-info"
  data-toggle="tooltip" data-placement="top" title="In stock: ${quantity}">${name}<br>${price.toFixed(
      2
    )} $</button>`;
}

function billTemplate(name: string, price: number, quantity: number): string {
  return `<div id='${name}' class="bill_row">
                    <div class="plus col-md-1">+</div>
                    <div class="product_name_in_bill col-md-7">${name} ${price.toFixed(
      2
    )}</div>
                    <div class="product_quantity_in_bill col-md-1">${quantity}</div>
                    <div class="minus col-md-1">-</div>
                </div>`;
}

function salesTemplate(productsList: string[], price: string, saleDate: string): string {
  const date: Date = new Date(+saleDate);

  return `<li>${productsList} price: ${price} Date: ${date.getDay()} / ${date.getMonth()} / ${date.getFullYear()}</li>`;
}

function productsTemplate(name: string, quantity: string): string {
  return `<li>${name} ${quantity}</li>`;
}

function ingredientTemplate(name: string, quantity: string): string {
  return `<li>${name} ${quantity}</li>`;
}

function recipeTemplate(name: string, ingredients: string[]): string {
  return `<li>${name} ${ingredients.join(' ')}</li>`;
}

export { buttonTemplate, billTemplate, salesTemplate, productsTemplate, ingredientTemplate, recipeTemplate };
