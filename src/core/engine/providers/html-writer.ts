// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';

import { injectable } from 'inversify';
import jquery from 'jquery';
import { IWriter } from '../../contracts/core/engine/providers/writer';

@injectable()
export class HTMLWriter implements IWriter {
  public write(id: string, content: string): void {
    jquery(`#${id}`).append(content);
  }
}
