import { inject, injectable } from 'inversify';
// tslint:disable-next-line:import-name
import $ from 'jquery';
import { TYPES } from '../../../common/types';
import { IBaseReport } from '../../contracts/core/engine/factories/base-report';
import { MainService } from '../../services/main-service';
import { IngredientReport } from '../factories/ingredient-report';
import { RecipeReport } from '../factories/recipe-report';
import { SaleReport } from '../factories/sale-report';
import { buttonTemplate } from '../providers/template';

import { ProductReport } from './../factories/product-report';

@injectable()
export class Controller {
  private readonly _mainService: MainService;
  private readonly _saleReport: IBaseReport;
  private readonly _productReport: IBaseReport;
  private readonly _ingredientReport: IBaseReport;
  private readonly _recipeReport: IBaseReport;

  constructor(
    @inject(TYPES.MainService) mainService: MainService,
    @inject(TYPES.SaleReport) saleReport: SaleReport,
    @inject(TYPES.ProductReport) productReport: ProductReport,
    @inject(TYPES.IngredientReport) ingredientReport: IngredientReport,
    @inject(TYPES.RecipeReport) recipeReport: RecipeReport
  ) {
    this._mainService = mainService;
    this._saleReport = saleReport;
    this._productReport = productReport;
    this._ingredientReport = ingredientReport;
    this._recipeReport = recipeReport;
  }

  public get mainService(): MainService {
    return this._mainService;
  }
  public get saleReport(): IBaseReport {
    return this._saleReport;
  }
  public get productReport(): IBaseReport {
    return this._productReport;
  }

  public get ingredientReport(): IBaseReport {
    return this._ingredientReport;
  }

  public get recipeReport(): IBaseReport {
    return this._recipeReport;
  }
  public numpadListener(): void {
    // tslint:disable-next-line:typedef
    $('#numpad').on(
      'click',
      '.numpad',
      (event: JQuery.Event<HTMLElement>): void => {
        const currentMultiplier: string =
          $('#multiplier').val() > 0
            ? $('#multiplier')
                .val()
                .toString()
            : '';
        $('#multiplier').val(currentMultiplier + event.target.textContent);
      }
    );
  }

  public footerNavigationButtonsListener(): void {
    $('.production-btn').click(() => {
      this.mainService.productionProductFiller();
      this.mainService.showProductionHtml();
    });
    $('.reports-btn').click(() => {
      this.mainService.showReportsHtml();
    });
    $('.pos-btn').click(() => {
      this.mainService.showPosHtml();
    });
  }

  public productButtonsListener(): void {
    $('#products').on('click', '.product-btn', (event: JQueryEventObject) => {
      this.mainService.addProductRecordToBill(event.target.innerHTML);
    });
  }

  public changeBillListener(): void {
    $('#bill').bind('DOMSubtreeModified', () => {
      let totalSum: number = 0;
      $('.bill_row').each(function(): void {
        totalSum +=
          +$(this)
            .children('.product_name_in_bill')
            .text()
            .split(' ')[1] *
          +$(this)
            .children('.product_quantity_in_bill')
            .text();
        $('#totalSum').val(`${totalSum.toFixed(2)} $`);
      });
    });
  }

  public clearBillListener(): void {
    $('#btn_void').on('click', () => {
      $('.bill_row').remove();
      $('#totalSum').val(0);
    });
  }

  public productDropDownListener(): void {
    $('#product-drpdwn').on(
      'change',
      (): void => {
        const option: string = $('#product-drpdwn')
          .val()
          .toString();
        this.mainService.productionRecipeFiller(option);
      }
    );
  }
  public recipeOptionListener(): void {
    $('#recipe-drpdwn').on(
      'change',
      (): void => {
        const option: string = $('#recipe-drpdwn')
          .val()
          .toString();
        this.mainService.productionListFiller(option);
      }
    );
  }
  public productionQuantityListener(): void {
    $('#production-quantity').on(
      'keyup',
      (): void => {
        const option: number = +$('#production-quantity').val();
        this.mainService.productionIngredientMultiplier(option);
      }
    );
  }

  public payBillListener(): void {
    $('#btn_pay').on('click', () => {
      if ($('#bill').children('.bill_row').length <= 0) {
        return;
      }
      const productsList: string[] = [];
      $('#bill')
        .children('.bill_row')
        .each(function(): void {
          const record: string = `${$(this)
            .children('.product_name_in_bill')
            .text()} ${+$(this)
            .children('.product_quantity_in_bill')
            .text()}`;

          productsList.push(record);
        });
      const totalSum: number = +$('#totalSum')
        .val()
        .toString()
        .split(' ')[0];

      this.mainService.makeRecordInSalesDb(productsList, totalSum);

      productsList.forEach((elem: string) => {
        const billRecordArgs: string[] = elem.split(' ');
        this.mainService.updateQuantityOfProduct(
          billRecordArgs[0],
          0 - +billRecordArgs[2]
        );
      });
      $('.bill_row').remove();
      $('#totalSum').val(0);
    });
  }

  public clearReportContainer(): void {
    $('#clear_report_id').on('click', () => {
      $('#report_container_id').empty();
    });
  }

  public saleReportListener(): void {
    $('#sales_id').on('click', () => {
      $('#report_container_id').empty();
      this.saleReport.getReport();
    });
  }

  public productReportListener(): void {
    $('#product_id').on('click', () => {
      $('#report_container_id').empty();
      this.productReport.getReport();
    });
  }

  public ingredientReportListener(): void {
    $('#ingredient_id').on('click', () => {
      $('#report_container_id').empty();
      this.ingredientReport.getReport();
    });
  }

  public recipesReportListener(): void {
    $('#recipe_id').on('click', () => {
      $('#report_container_id').empty();
      this.recipeReport.getReport();
    });
  }
  public createProductLIstener(): void {
    $('#btn_create').on('click', () => {
      this.mainService.createProduct();
    });
  }

  public productionClearListener(): void {
    $('#btn_clear').on('click', () => {
      $('.list').empty();
      $('#product-drpdwn').val('Product');
      $('#recipe-drpdwn').val('Recipe');
      $('#production-quantity').val(0);
    });
  }

  public listen(): void {
    this.productButtonsListener();
    this.changeBillListener();
    this.footerNavigationButtonsListener();
    this.clearBillListener();
    this.payBillListener();
    this.numpadListener();
    this.productDropDownListener();
    this.recipeOptionListener();
    this.productionQuantityListener();
    this.clearReportContainer();
    this.saleReportListener();
    this.productReportListener();
    this.ingredientReportListener();
    this.recipesReportListener();
    this.productionClearListener();
    this.createProductLIstener();
  }

  public addProductsButtonsToTheCategory(
    // tslint:disable-next-line:no-any
    docss: any[],
    filter: string
  ): string {
    let output: string = '';

    docss
      // tslint:disable-next-line:no-any
      .filter((elem: any) => elem._productGroup === filter)
      .forEach(
        // tslint:disable-next-line:no-any
        (elem: any) =>
          (output += buttonTemplate(
            elem._recipeName.toString(),
            elem._productPrice,
            elem._productQuantity
          ))
      );

    return output;
  }
}
