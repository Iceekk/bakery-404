// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';

import { remote } from 'electron';
import { inject, injectable } from 'inversify';
import nedb from 'nedb';
import { TYPES } from '../../common/types';
import { IWriter } from './../contracts/core/engine/providers/writer';
import { IEngine } from './../contracts/engine';
import { Controller } from './controlers/main-controller';

@injectable()
export class Engine implements IEngine {
  private readonly writer: IWriter;
  private readonly controller: Controller;

  constructor(
    @inject(TYPES.HTMLWriter) writer: IWriter,
    @inject(TYPES.Controller) controller: Controller
  ) {
    this.writer = writer;
    this.controller = controller;
  }

  public start(): void {
    const inStock: nedb = remote.getGlobal('vars').inStock;

    // tslint:disable-next-line:no-any
    inStock.find({}, (err: string, docs: any[]) => {
      if (err) {
        throw new Error(err);
      }

      this.writer.write('easter-cake', this.controller.addProductsButtonsToTheCategory(docs, 'EASTER_CAKE'));
      this.writer.write('banitsas', this.controller.addProductsButtonsToTheCategory(docs, 'BANITSA'));
      this.writer.write('breads', this.controller.addProductsButtonsToTheCategory(docs, 'BREAD'));
      this.writer.write('buns', this.controller.addProductsButtonsToTheCategory(docs, 'MUFFIN'));
    });

    this.controller.listen();
  }
}
