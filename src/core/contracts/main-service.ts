import { HTMLWriter } from './../engine/providers/html-writer';
export interface IService {
  addProductRecordToBill(event: string, writer: HTMLWriter): void;
  htmlShow(element: string): void;
  htmlHide(element: string): void;
}
