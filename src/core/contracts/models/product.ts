import { ProductType } from '../../../models/enums/product-type';
import { Recipe } from '../../../models/recipe';

export interface IProduct {
  productGroup: ProductType;
  productRecipe: Recipe;
  productPrice: number;
  productQuantity: number;
}
