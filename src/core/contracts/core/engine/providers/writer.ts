export interface IWriter {
    write(id: string, content: string): void;
}
