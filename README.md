# Bakery 404

1. Team name: Bakery 404

2. Team members: iceek, oum.aleksandr@g, Dimitar.Vutov

3. Project: the project is about Bakery and he is focusing on back-end logic and the best practices in object-oriented programming.

4. URL of Git repository: https://gitlab.com/Iceekk/bakery-404

5. Features: Desktop app for selling bakeries.

6. First run:

- **npm install**
- **npm run build**
- **npm run run-electron**
