// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { IWriter } from '../src/core/contracts/core/engine/providers/writer';
import { Controller } from '../src/core/engine/controlers/main-controller';
import { BaseReport } from '../src/core/engine/factories/abstract/base-report';
import { ProductReport } from '../src/core/engine/factories/product-report';
import { RecipeReport } from '../src/core/engine/factories/recipe-report';
import { SaleReport } from '../src/core/engine/factories/sale-report';
import { HTMLWriter } from '../src/core/engine/providers/html-writer';
import { IngredientReport } from './../src/core/engine/factories/ingredient-report';
import { MainService } from './../src/core/services/main-service';
describe('MAIN CONTROLLER', () => {
    beforeEach(() => {
        jest.mock('./../src/core/services/main-service');
        jest.mock('../src/core/engine/providers/html-writer');
        jest.mock('../src/core/engine/factories/sale-report');
        jest.mock('../src/core/engine/factories/product-report');
        jest.mock('../src/core/engine/factories/ingredient-report');
        jest.mock('../src/core/engine/factories/recipe-report');
    });
    describe('constructor method should ', () => {
        it('set main service appropriately', () => {
            // Act & Arrange
            const mainService: MainService = new MainService();
            const writer: IWriter = new HTMLWriter();
            const salesReport: BaseReport = new SaleReport(writer);
            const productReport: BaseReport = new ProductReport(writer);
            const ingredientReport: BaseReport = new IngredientReport(writer);
            const recipeReport: BaseReport = new RecipeReport(writer);
            const controller: Controller = new Controller(mainService, salesReport, productReport, ingredientReport, recipeReport);
            // Assert
            expect(controller.mainService).toEqual(mainService);
        });
        it('set sales report appropriately', () => {
            // Act & Arrange
            const mainService: MainService = new MainService();
            const writer: IWriter = new HTMLWriter();
            const salesReport: BaseReport = new SaleReport(writer);
            const productReport: BaseReport = new ProductReport(writer);
            const ingredientReport: BaseReport = new IngredientReport(writer);
            const recipeReport: BaseReport = new RecipeReport(writer);
            const controller: Controller = new Controller(mainService, salesReport, productReport, ingredientReport, recipeReport);
            // Assert
            expect(controller.saleReport).toEqual(salesReport);
        });
        it('set product report appropriately', () => {
             // Act & Arrange
             const mainService: MainService = new MainService();
             const writer: IWriter = new HTMLWriter();
             const salesReport: BaseReport = new SaleReport(writer);
             const productReport: BaseReport = new ProductReport(writer);
             const ingredientReport: BaseReport = new IngredientReport(writer);
             const recipeReport: BaseReport = new RecipeReport(writer);
             const controller: Controller = new Controller(mainService, salesReport, productReport, ingredientReport, recipeReport);
             // Assert
             expect(controller.productReport).toEqual(productReport);
        });
        it('set ingredient report appropriately', () => {
            // Act & Arrange
            const mainService: MainService = new MainService();
            const writer: IWriter = new HTMLWriter();
            const salesReport: BaseReport = new SaleReport(writer);
            const productReport: BaseReport = new ProductReport(writer);
            const ingredientReport: BaseReport = new IngredientReport(writer);
            const recipeReport: BaseReport = new RecipeReport(writer);
            const controller: Controller = new Controller(mainService, salesReport, productReport, ingredientReport, recipeReport);
            // Assert
            expect(controller.ingredientReport).toEqual(ingredientReport);
        });
        it('set recipe report appropriately', () => {
            // Act & Arrange
            const mainService: MainService = new MainService();
            const writer: IWriter = new HTMLWriter();
            const salesReport: BaseReport = new SaleReport(writer);
            const productReport: BaseReport = new ProductReport(writer);
            const ingredientReport: BaseReport = new IngredientReport(writer);
            const recipeReport: BaseReport = new RecipeReport(writer);
            const controller: Controller = new Controller(mainService, salesReport, productReport, ingredientReport, recipeReport);
            // Assert
            expect(controller.recipeReport).toEqual(recipeReport);
        });
    });
});
