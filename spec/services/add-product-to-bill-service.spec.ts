/**
 * @jest-environment jsdom
 */

import $ from 'jquery';
import 'reflect-metadata';
import { IWriter } from '../../src/core/contracts/core/engine/providers/writer';
import { HTMLWriter } from '../../src/core/engine/providers/html-writer';
import { AddProductRecordToBill } from './../../src/core/services/add-product-to-bill-service';
jest.mock('../../src/core/engine/providers/html-writer.ts');

beforeAll(() => {
  // tslint:disable-next-line:no-inner-html
  document.body.innerHTML = `<input id="multiplier"><div id="bill">
    <div id="MARMALADE_MUFFIN" class="bill_row">
    <div class="product_name_in_bill col-md-7">MARMALADE_MUFFIN 6.00</div>
    <div class="product_quantity_in_bill col-md-1">7</div>
    </div>`;
});

describe('Add product record to bill service', () => {
  describe('method addRowToBill() should', () => {
    it('reset multiplier input value', () => {
      // Arrange
      const mockWriter: IWriter = new HTMLWriter();
      const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
        mockWriter
      );
      const dataFromButton: string = 'MARMALADE_MUFFIN<br>6.00';
      // Act
      $('#multiplier').val(5);
      billManipulator.addRowToBill(dataFromButton);
      // Assert
      expect($('#multiplier').val()).toBe('0');
    });
    it('call resetHtmlInputValue() 1 time', () => {
      // Arrange
      const mockWriter: IWriter = new HTMLWriter();
      const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
        mockWriter
      );
      const dataFromButton: string = 'MARMALADE_MUFFIN<br>6.00';
      const spy: jest.SpyInstance = jest.spyOn(
        billManipulator,
        'resetHtmlInputValue'
      );
      // Act
      $('#multiplier').val(5);
      billManipulator.addRowToBill(dataFromButton);
      // Assert
      expect(spy).toBeCalledTimes(1);
    });
    it('call checkSameRecordInBill()', () => {
      // Arrange
      const mockWriter: IWriter = new HTMLWriter();
      const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
        mockWriter
      );
      const dataFromButton: string = 'MARMALADE_MUFFIN<br>6.00';
      const spy: jest.SpyInstance = jest.spyOn(
        billManipulator,
        'checkSameRecordInBill'
      );
      // Act
      $('#multiplier').val(5);
      billManipulator.addRowToBill(dataFromButton);
      // Assert
      expect(spy).toBeCalled();
    });
  });
  describe('method insertNewRecord() should', () => {
    it('call writer write() method 1 time', () => {
      // Arrange
      const mockWriter: IWriter = new HTMLWriter();
      const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
        mockWriter
      );
      const dataFromButton: string = 'BREAD<br>3.00';
      const spy: jest.SpyInstance = jest.spyOn(mockWriter, 'write');
      // Act
      $('#multiplier').val(5);
      billManipulator.addRowToBill(dataFromButton);
      // Assert
      expect(spy).toBeCalledTimes(1);
    });
    it('add new line in bill', () => {
      // Arrange
      const mockWriter: IWriter = new HTMLWriter();
      const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
        mockWriter
      );
      const dataFromButton: string = 'BANITSA<br>1.00';
      const spy: jest.SpyInstance = jest.spyOn(mockWriter, 'write');
      const numberOfRowsInBill: number = $('.bill_row').length;
      // Act
      $('#multiplier').val(2);
      billManipulator.addRowToBill(dataFromButton);
      // Assert
      expect(1).toBe(1);
    });
    describe('method checkSameRecordInBill() should', () => {
      it('call getValueFromHtmlInputField() method', () => {
        // Arrange
        const mockWriter: IWriter = new HTMLWriter();
        const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
          mockWriter
        );
        const dataFromButton: string = 'BREAD<br>3.00';
        const spy: jest.SpyInstance = jest.spyOn(
          billManipulator,
          'getValueFromHtmlInputField'
        );
        // Act
        $('#multiplier').val(5);
        billManipulator.addRowToBill(dataFromButton);
        // Assert
        expect(spy).toBeCalled();
      });
      it('call updateExistingRow() method if bill already containe product', () => {
        // Arrange
        const mockWriter: IWriter = new HTMLWriter();
        const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
          mockWriter
        );
        const dataFromButton: string = 'MARMALADE_MUFFIN<br>6.00';
        const spy: jest.SpyInstance = jest.spyOn(
          billManipulator,
          'updateExistingRow'
        );
        // Act
        $('#multiplier').val(5);
        billManipulator.addRowToBill(dataFromButton);
        // Assert
        expect(spy).toBeCalled();
      });
      it('d`not call updateExistingRow() method if product is not in bill', () => {
        // Arrange
        const mockWriter: IWriter = new HTMLWriter();
        const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
          mockWriter
        );
        const dataFromButton: string = 'MARMALADE<br>6.00';
        const spy: jest.SpyInstance = jest.spyOn(
          billManipulator,
          'updateExistingRow'
        );
        // Act
        $('#multiplier').val(5);
        billManipulator.addRowToBill(dataFromButton);
        // Assert
        expect(spy).toBeCalledTimes(0);
      });
    });
    describe('method updateExistingRow() should', () => {
      it('increment existing product quantity in bill', () => {
        // Arrange
        const mockWriter: IWriter = new HTMLWriter();
        const billManipulator: AddProductRecordToBill = new AddProductRecordToBill(
          mockWriter
        );
        const dataFromButton: string = 'MARMALADE_MUFFIN<br>6.00';
        const existingQuantity: string = $('.product_quantity_in_bill').text();
        // Act
        $('#multiplier').val('3');
        const multiplier: string = $('#multiplier')
          .val()
          .toString();
        billManipulator.addRowToBill(dataFromButton);
        // Assert
        expect($('.product_quantity_in_bill').text()).toBe(
          (+existingQuantity + +multiplier).toString()
        );
      });
    });
  });
});
