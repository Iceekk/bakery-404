// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { Ingredient } from './../../src/models/ingredient';
describe('INGREDIENT MODEL', () => {
    describe('Constructor:', () => {
        it('should throw Error when ingredient name value is invalid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);

            // Act & Assert
            expect(() => { ingredient.ingredientName = null; }).toThrowError('Ingredient name is invalid!');

        });
        it('should throw Error when ingredient quantity value is invalid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);

            // Act & Assert
            expect(() => { ingredient.ingredientQuantity = -5; }).toThrowError('Ingredient quantity is invalid!');

        });
        it('should correctly assign passed values', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);

            // Act & Assert
            expect(ingredient.ingredientQuantity).toEqual(100);
            expect(ingredient.ingredientName).toEqual('chocolate');
            expect(ingredient.ingredientExpirationDate).toEqual(new Date('2011-12-29T22:00:00.000Z'));

        });
        it('should not to throw Error when passed value is valid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);

            // Act & Assert
            expect(() => { ingredient.ingredientName = 'vanilia'; }).not.toThrowError();

        });
    });
});
