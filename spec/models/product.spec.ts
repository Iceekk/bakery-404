// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { IProduct } from '../../src/core/contracts/models/product';
import { RecipeType } from '../../src/models/enums/recipe-type';
import { Product } from '../../src/models/product';
import { Recipe } from '../../src/models/recipe';
import { ProductType } from './../../src/models/enums/product-type';
import { Ingredient } from './../../src/models/ingredient';

describe('PRODUCT MODEL', () => {
    describe('Constructor:', () => {
        it('should throw Error when product price value is invalid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);
            const product: IProduct = new Product(ProductType.BANITSA, recipe, 5, 50);

            // Act & Assert
            expect(() => { product.productPrice = null; }).toThrowError('Product price is invalid!');

        });
        it('should not throw Error when product price value is valid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);
            const product: IProduct = new Product(ProductType.BANITSA, recipe, 5, 50);

            // Act & Assert
            expect(() => { product.productPrice = 5; }).not.toThrowError();

        });
        it('should throw Error when product quantity value is invalid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);
            const product: IProduct = new Product(ProductType.BANITSA, recipe, 5, 50);

            // Act & Assert
            expect(() => { product.productQuantity = null; }).toThrowError('Product quantity is invalid!');

        });
        it('should not throw Error when product quantity value is valid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);
            const product: IProduct = new Product(ProductType.BANITSA, recipe, 5, 50);

            // Act & Assert
            expect(() => { product.productQuantity = 5; }).not.toThrowError();

        });
        it('should correctly assign passed values', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);

            // Act
            const product: IProduct = new Product(ProductType.BANITSA, recipe, 5, 50);

            // Assert
            expect(product.productGroup).toBe('BANITSA');
            expect(product.productRecipe).toBeInstanceOf(Recipe);
            expect(product.productPrice).toBe(5);
            expect(product.productQuantity).toBe(50);

        });
    });
});
