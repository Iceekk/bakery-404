// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { RecipeType } from '../../src/models/enums/recipe-type';
import { Recipe } from '../../src/models/recipe';
import { ProductType } from './../../src/models/enums/product-type';
import { Ingredient } from './../../src/models/ingredient';

describe('RECIPE MODEL', () => {
    describe('Constructor:', () => {
        it('should correctly assign passed values', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const ingredient: Ingredient = new Ingredient('chocolate', 100, date);

            // Act
            const recipe: Recipe = new Recipe(ProductType.BANITSA, RecipeType.CHEESE_BANITSA, [ingredient]);

            // Assert
            expect(recipe.recipeName).toBe('CHEESE_BANITSA');
            expect(recipe.typeRecipe).toBe('BANITSA');
            expect(recipe.recipeIngrediants).toBeInstanceOf(Array);

        });
    });
});
