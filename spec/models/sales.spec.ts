// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { SaleModel } from './../../src/models/sales-model';

describe('SALE MODEL', () => {
    describe('Constructor:', () => {
        it('should throw Error when total sum value is invalid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const saleModel: SaleModel = new SaleModel(['test'], 50, date);

            // Act & Assert
            expect(() => { saleModel.totalSum = null; }).toThrowError('Invalid total sum!');

        });
        it('should not throw Error when total sum value is valid', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const saleModel: SaleModel = new SaleModel(['test'], 50, date);

            // Act & Assert
            expect(() => { saleModel.totalSum = 5; }).not.toThrowError('Product price is invalid!');

        });
        it('should throw Error when products list value is empty', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const saleModel: SaleModel = new SaleModel(['test'], 50, date);

            // Act & Assert
            expect(() => { saleModel.productsList = []; }).toThrowError('Products list is empty!');

        });
        it('should not throw Error when products list value is not empty', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);
            const saleModel: SaleModel = new SaleModel(['test'], 50, date);

            // Act & Assert
            expect(() => { saleModel.productsList = ['check']; }).not.toThrowError();

        });
        it('should correctly assign passed values', () => {

            // Arrange
            const date: Date = new Date(2011, 11, 30);

            // Act
            const saleModel: SaleModel = new SaleModel(['test'], 50, date);

            // Assert
            expect(saleModel.productsList).toBeInstanceOf(Array);
            expect(saleModel.totalSum).toBe(50);
            expect(saleModel.dateOfSale).toBe(date);

        });
    });
});
