
// tslint:disable-next-line
import 'reflect-metadata';
import { IEngine } from '../src/core/contracts/engine';
import { BaseReport } from '../src/core/engine/factories/abstract/base-report';
import { IngredientReport } from '../src/core/engine/factories/ingredient-report';
import { RecipeReport } from '../src/core/engine/factories/recipe-report';
import { SaleReport } from '../src/core/engine/factories/sale-report';
import { Controller } from './../src/core/engine/controlers/main-controller';
import { ProductReport } from './../src/core/engine/factories/product-report';
import { HTMLWriter } from './../src/core/engine/providers/html-writer';
import { MainService } from './../src/core/services/main-service';

describe('Engine class', () => {

  beforeEach(() => {

    jest.mock('./../src/core/services/main-service.ts');
    jest.mock('./../src/core/engine/controlers/main-controller.ts');
    jest.mock('./../src/core/engine/providers/html-writer.ts');

    jest.mock('./../src/core/engine/factories/ingredient-report.ts');
    jest.mock('./../src/core/engine/factories/product-report.ts');
    jest.mock('./../src/core/engine/factories/recipe-report.ts');
    jest.mock('./../src/core/engine/factories/sale-report.ts');

  });

  describe('start method should', () => {
    it('call the start method once', () => {

      // Arrange
      const mockEngine: jest.Mock<IEngine> = jest
        .fn<IEngine>()
        .mockImplementation(() => ({
          start: jest.fn().mockReturnValue('Wrooom Wrooom')
        }));
      const mainService: MainService = new MainService();
      const writer: HTMLWriter = new HTMLWriter();
      const saleReport: BaseReport = new SaleReport(writer);
      const productReport: BaseReport = new ProductReport(writer);
      const ingredientReport: BaseReport = new IngredientReport(writer);
      const recipeReport: BaseReport = new RecipeReport(writer);
      const controller: Controller = new Controller(mainService, saleReport, productReport, ingredientReport, recipeReport);
      const engine: IEngine = new mockEngine(writer, controller);
      const spy: jest.SpyInstance = jest.spyOn(engine, 'start');

      // Act
      engine.start();

      // Assert
      expect(spy).toBeCalledTimes(1);

    });
  });
});
