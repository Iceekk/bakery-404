/**
 * @jest-environment jsdom
 */
//import * as $ from 'jquery';
// tslint:disable-next-line:max-line-length
import {
  billTemplate,
  buttonTemplate,
  ingredientTemplate,
  productsTemplate,
  recipeTemplate,
  salesTemplate
} from '../src/core/engine/providers/template';
describe('TEMPLATES', () => {
  describe('Button template should', () => {
    it('create button template', () => {
      //  Act & Arrange & Assert
      expect(buttonTemplate('pesho', 15, 20))
        .toBe(`<button class="product-btn btn btn-info"
  data-toggle="tooltip" data-placement="top" title="In stock: 20">pesho<br>15.00 $</button>`);
    });
  });
  describe('Bill template should', () => {
    it('create bill template', () => {
      //  Act & Arrange & Assert
      expect(billTemplate('tosho', 15, 20))
        .toBe(`<div id='tosho' class="bill_row">
                    <div class="plus col-md-1">+</div>
                    <div class="product_name_in_bill col-md-7">tosho 15.00</div>
                    <div class="product_quantity_in_bill col-md-1">20</div>
                    <div class="minus col-md-1">-</div>
                </div>`);
    });
  });
  describe('Sales template should', () => {
    it('create a sale template with name, price, date', () => {
      // Arrange
      const date: Date = new Date();
      // tslint:disable-next-line:max-line-length
      // Act & Assert
      expect(salesTemplate(['pesho', 'tosho'], '12', '')).toBe(
        `<li>pesho,tosho price: 12 Date: 4 / 0 / 1970</li>`
      );
    });
  });
  describe('productsTemplate should', () => {
    it('create product template', () => {
      //  Act & Arrange & Assert
      expect(productsTemplate('pesho', '12')).toBe(`<li>pesho 12</li>`);
    });
  });
  describe('ingredientTemplate should', () => {
    it('create ingredient template', () => {
      //  Act & Arrange & Assert
      expect(ingredientTemplate('pesho', '22')).toBe(`<li>pesho 22</li>`);
    });
  });
  describe('recipeTemplate should', () => {
    it('create recipe template', () => {
      //  Act & Arrange & Assert
      expect(recipeTemplate('pesho', ['gosho', 'tosho'])).toBe(
        `<li>pesho gosho tosho</li>`
      );
    });
  });
});
